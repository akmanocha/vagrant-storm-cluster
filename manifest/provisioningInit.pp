$CLONE_URL = "https://bitbucket.org/qanderson/storm-puppet.git"
$CHECKOUT_DIR="/tmp/storm-puppet"
package
package
package
package
package
{git:ensure=> [latest,installed]}
{puppet:ensure=> [latest,installed]}
{ruby:ensure=> [latest,installed]}
{rubygems:ensure=> [latest,installed]}
{unzip:ensure=> [latest,installed]}
exec { "install_hiera":
command => "gem install hiera hiera-puppet",
path => "/usr/bin",
require => Package['rubygems'],
}
exec { "clone_storm-puppet":
command => "git clone ${CLONE_URL}",
cwd => "/tmp",
path => "/usr/bin",
creates => "${CHECKOUT_DIR}",
require => Package['git'],
}
exec {"/bin/ln -s /var/lib/gems/1.8/gems/hiera-puppet-1.0.0/ /tmp/
storm-puppet/modules/hiera-puppet":
creates => "/tmp/storm-puppet/modules/hiera-puppet",
require => [Exec['clone_storm-
puppet'],Exec['install_hiera']]
}
#install hiera and the storm configuration
file { "/etc/puppet/hiera.yaml":
source => "/vagrant_data/hiera.yaml",
replace => true,
require => Package['puppet']
}
file { "/etc/puppet/hieradata":
ensure => directory,
require => Package['puppet']
}
file {"/etc/puppet/hieradata/storm.yaml":
source => "${CHECKOUT_DIR}/modules/storm.yaml",
replace => true,
require => [Exec['clone_storm-puppet'],File['/etc/puppet/
hieradata']]
}

